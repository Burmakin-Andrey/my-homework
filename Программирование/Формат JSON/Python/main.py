import requests
import sys

api_url = "http://api.openweathermap.org/data/2.5/weather"
try:
  city = input("Введите город: ")

  while True:
    units = input("Введите систему мер (М - метрическая, И - империческая): ")
    if (units.lower() == "м"):
      units = "metric"
      break     
    elif (units.lower() == "и"):
      units = "imperial"
      break

  appid = "68cbf275963f61bc9aeb6710080abd0e"

  params = {"appid": appid, "q": city, 'units': units, "lang": "ru"}

  weather_get = requests.get(api_url,
  params = params)
  data = weather_get.json()
  lon = data["coord"]["lon"]
  lat = data["coord"]["lat"]
except KeyError:
  print("Такого города нет в базе.")
  sys.exit()
api_url_onecall = f'https://api.openweathermap.org/data/2.5/onecall?lat={lat}&lon={lon}&exclude="daily"&units={units}&appid={appid}'

params = {"appid": appid, "lat": lat, "lon": lon, 'units': units, "lang": "ru", "exclude": "daily"}

weather_get = requests.get(api_url_onecall,
params = params)
data = weather_get.json()

for i in range(0, len(data)):
  print(f"День {i + 1}:")

  print("\tТемпература:")
  print("\t\tУтро: ", round(data["daily"][i]["temp"]["morn"]))
  print("\t\tДень: ", round(data["daily"][i]["temp"]["day"]))
  print("\t\tВечер: ", round(data["daily"][i]["temp"]["eve"]))
  print("\t\tНочь: ", round(data["daily"][i]["temp"]["night"]))

  print("\tТемпература по ощущениям:")
  print("\t\tУтро: ", round(data["daily"][i]["feels_like"]["morn"]))
  print("\t\tДень: ", round(data["daily"][i]["feels_like"]["day"]))
  print("\t\tВечер: ", round(data["daily"][i]["feels_like"]["eve"]))
  print("\t\tНочь: ", round(data["daily"][i]["feels_like"]["night"]))

  print("\tОсадки: ", round(data["daily"][i]["clouds"]))

  print("\tСкорость ветра: ", round(data["daily"][i]["wind_speed"], 1), "m/s")

  print("\tВлажность: ", round(data["daily"][i]["humidity"], 1), "%")