class Heaps:
  def __init__(self):
    self._que = []
    self.MinMax = "min"
    self.isfull = True

  #Добавление элементов из другого массива (не знаю зачем сделал, можно и просто через heap._que = [эл-ты] реализовать, но я подумал почему нет)  
  def heappush(self, arr):
    for i in arr:
      self._que.append(i)

  #Алгоритм сортировки
  def heapify(self, n, i):
    HeapMax = i
    left = 2 * i + 1 
    right = 2 * i + 2

    if left < n and self._que[left] > self._que[HeapMax]:
        HeapMax = left

    if right < n and self._que[right] > self._que[HeapMax]:
        HeapMax = right

    if HeapMax != i:
        self._que[i],self._que[HeapMax] = self._que[HeapMax],self._que[i]
        self.heapify(n, HeapMax)

  def heapSort(self):
    n = len(self._que)

    for i in range(n, -1, -1):
        self.heapify(n, i)

    for i in range(n-1, 0, -1):
        self._que[i], self._que[0] = self._que[0], self._que[i]
        self.heapify(i, 0)

  #Проверка на существование обоих детей одновременно
  def Children(self):
    for i in range(len(self._que)):
      leftChild = 2 * i + 1
      rightChild = 2 * i + 2

      if ((rightChild < len(self._que) and leftChild >= len(self._que)) or (rightChild >= len(self._que) and leftChild < len(self._que))):
        self.isfull = False

  #Смена Мин кучи на Макс кучу и наоборот
  def MinMaxSwap(self):
    self._que.reverse()
    if (self.MinMax == "min"):
      self.MinMax = "max"
    else:
      self.MinMax = "min"

  #Вывод состояния полноты дерева
  def IsFull(self):
    self.Children()
    if self.isfull == True:
      print("Дерево полное")
    else:
      print("Дерево неполное")


#Вывод в виде дерева
def print_heapify(ar):
    ml = max(len(str(x)) for x in ar)
    ars = [('{:0'+str(ml)+'}').format(x) for x in ar]
    dp = len(bin(len(ar))) - 1
    print('*' * 2**(dp-2) * (ml + 1))
    for i in range(1, dp + 1):
        str_space = ' ' * max(0, 2**(dp-i-2) * (ml + 1) - 1 - ml // 2)
        sep_space = ' ' * max(0, 2**(dp-i-1) * (ml + 1) - ml)
        print(str_space + sep_space.join(ars[2**(i-1) - 1: 2**i - 1]))
    print('*' * 2**(dp-2) * (ml + 1))


arr = [2, 3, 5, 1, 4, 6, 8, 9, 0, 7, 10, 13, 14, 12, 11]
heap = Heaps()

heap.heappush(arr)

print("Неотсортированный список: ", heap._que, "\n\nНеотсортированное дерево: \n")
print_heapify(heap._que)

heap.heapSort()

print("Отсортированный список: ", heap._que, "\n\nОтсортированное дерево: \n")
print_heapify(heap._que)

heap.IsFull()