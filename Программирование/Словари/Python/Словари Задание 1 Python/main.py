import re
text_dict = dict()
text = "Прочитайте текст, используйте словарь для подсчета вхождений каждого слова, а затем выведите самые частые и самые редкие слова."
text = re.sub(r'[^\w\s]','',text).lower().split()
for i in text:
  if i in text_dict:
    text_dict[i] += 1
  else:
    text_dict[i] = 1
maxKey = list(text_dict.values())[0]
maxWord = list(text_dict.keys())[0]
minKey = list(text_dict.values())[0]
minWord = list(text_dict.keys())[0]
for j in list(text_dict.keys()):
    print(j, ":", text_dict[j])
    if text_dict[j] > maxKey:
      maxKey = text_dict[j]
      maxWord = j
    if text_dict[j] < minKey:
      minKey = text_dict[j]
      minWord = j
print("Самое частое слово:\n",  maxWord, ":", maxKey,"\nСамое редкое слово:\n",  minWord, ":", minKey)