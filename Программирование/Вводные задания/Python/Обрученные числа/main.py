print ("Два числа n и m называются обрученными, если каждое из них равно сумме делителей второго числа плюс 1 (без учета самих чисел) \nЗадача: \n Найти все пары обрученных чисел в промежутке от 1 до 10 000.")
i = 1
second_num = 0
while i < 10000:
  dividers_n, dividers_m = 0, 0
  for j in range (i - 1, 0, -1):
    if (i % j) == 0:
      dividers_n += j
    second_num = dividers_n-1
  for j in range (second_num - 1, 0, -1):
    if (second_num % j) == 0:
      dividers_m += j
  if ((i + 1 == dividers_m) and (second_num + 1 == dividers_n) and (i != dividers_n) and(second_num < 10000)):
    print(i, " и ", second_num)
    i = second_num
  i += 1