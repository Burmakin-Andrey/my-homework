print("Два числа n и m называются дружественными, если каждое из них равно сумме делителей второго числа (без учета самих чисел) \nЗадача: \nНайти все пары дружественных чисел в промежутке от 1 до 10 000")
i = 1
while i < 10000:
  dividers_n, dividers_m = 0, 0
  for j in range (i - 1, 0, -1):
    if (i % j) == 0:
      dividers_n += j
  for j in range (dividers_n - 1, 0, -1):
    if (dividers_n % j) == 0:
      dividers_m += j
  if (i == dividers_m) and (i != dividers_n):
    print(i, " и ", dividers_n)
    i = dividers_n
  i += 1