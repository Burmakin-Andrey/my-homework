#include <iostream>
#include <fstream>
#include <cstring>
#include <cmath>

using namespace std;

int main() {
  setlocale(LC_ALL, "Russian");
  cout << "Два числа n и m называются дружественными, если каждое из них равно сумме делителей второго числа (без учета самих чисел) \nЗадача: \nНайти все пары дружественных чисел в промежутке от 1 до 10 000" << endl;
  for (int i = 1; i < 10000; i++){
    int dividers_n = 0, dividers_m = 0;

    for (int j = i - 1; j > 0; j--){
      if (i % j == 0){
        dividers_n += j;
      }
    }

    for (int j = dividers_n - 1; j > 0; j--){
      if (dividers_n % j == 0){
        dividers_m += j;
      }
    }

    if ((i == dividers_m) && (i != dividers_n)){
      cout << i << " и " << dividers_n << endl;
      i = dividers_n;
    }

  }
}