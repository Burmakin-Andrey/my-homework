#include <iostream>
#include <fstream>
#include <cstring>
#include <cmath>

using namespace std;

int main() {
  setlocale(LC_ALL, "Russian");
  cout << "Два числа n и m называются обрученными, если каждое из них равно сумме делителей второго числа плюс 1 (без учета самих чисел) \nЗадача: \n Найти все пары обрученных чисел в промежутке от 1 до 10 000." << endl;
  for (int i = 1; i < 10000; i++){
    int dividers_n = 0, dividers_m = 0;

    for (int j = i - 1; j > 0; j--){
      if (i % j == 0){
        dividers_n += j;
      }
    }

    int second_num = dividers_n-1;

    for (int j = second_num - 1; j > 0; j--){
      if (second_num % j == 0){
        dividers_m += j;
      }
    }


    if ((i + 1 == dividers_m) && (second_num + 1 == dividers_n) && (i != dividers_n) && (second_num < 10000)){
      cout << i << " и " << second_num << endl;
      i = second_num;
    }

  }
}