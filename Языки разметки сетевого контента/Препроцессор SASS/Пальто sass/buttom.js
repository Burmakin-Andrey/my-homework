var bottom_position = 0;
var flag_bottom = false;
var flag_animate = false;
 
$(document).ready(function(){

    $('.in_top').click(function(){

        flag_animate = true;

        if(flag_bottom){

            $("body,html").animate({"scrollTop":bottom_position}, 300, function(){ 

                flag_animate = false;
            });

            flag_bottom = false;
            $('.in_top span').html(' Наверх ');
        }else{

            $("body,html").animate({"scrollTop":0}, 300, function(){ 
                flag_animate = false;
            });     

            bottom_position = $(window).scrollTop();

            flag_bottom = true;
            $('.in_top span').html(' Назад ');
        }
    });
  

    $(window).scroll(function(event){
        var countScroll = $(window).scrollTop();

        if (countScroll > 100 && !flag_animate){
            $('.in_top').show();
            if(flag_bottom){
                flag_bottom = false;
                $('.in_top span').html(' Наверх ');
            }

        }else{
            if(!flag_bottom){
                $('.in_top').hide();
            }
        }
    });
});