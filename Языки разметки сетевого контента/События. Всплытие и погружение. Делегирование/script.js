list.onclick = function(event) {
  if (event.target.tagName == 'SPAN') {
    let ulTarget = event.target.parentNode.querySelector('ul')
    if (ulTarget) {
      ulTarget.hidden = !ulTarget.hidden
    } else {
        return;
    }
  } else {
      return;
  }
}