field.onclick = function(event) {
    let fieldCoord = field.getBoundingClientRect();
    let leftCoord = event.clientX - fieldCoord.left - field.clientLeft;
    let imageCoord = {};

    imageCoord.top = event.clientY - fieldCoord.top - field.clientTop - image.clientHeight / 2
    imageCoord.bottom = imageCoord.top + image.clientHeight
    imageCoord.left = event.clientX - fieldCoord.left - field.clientLeft - image.clientWidth / 2
    imageCoord.right = imageCoord.left + image.clientWidth

    if (imageCoord.top < 0) {
      imageCoord.top = 0;
    }

    if (imageCoord.left < 0) {
      imageCoord.left = 0;
    }

    if (imageCoord.right > field.clientWidth) {
        imageCoord.left = field.clientWidth - image.clientWidth;
    }

    if (imageCoord.bottom > field.clientHeight) {
      imageCoord.top = field.clientHeight - image.clientHeight;
    }

    image.style.left = imageCoord.left + "px";
    image.style.top = imageCoord.top + "px";
}
